/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.pnu.jsonexample;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author developer
 */
public class Professor {
 
    private String firstName;       // Имя преподавателя
    private String sureName;        // Фамилия преподавателя
    private String middleName;      // Отчетсво преподавателя
    
    private String department;      // Кафедра 
    private String position;        // Должность

    
    // Список студентов - дипломников, закрепленных за преподавателем Professor
    private List<Student> graduatorList = new ArrayList<>();
    
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Преподаватель: {");
        sb.append("firstName=").append(firstName);
        sb.append(", sureName=").append(sureName);
        sb.append(", middleName=").append(middleName);
        sb.append(", department=").append(department);
        sb.append(", position=").append(position);
        sb.append('}');
        return sb.toString();
    }

    public List<Student> getGraduatorList() {
        return graduatorList;
    }

    public void setGraduatorList(List<Student> graduatorList) {
        this.graduatorList = graduatorList;
    }

    
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSureName() {
        return sureName;
    }

    public void setSureName(String sureName) {
        this.sureName = sureName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
    
}
