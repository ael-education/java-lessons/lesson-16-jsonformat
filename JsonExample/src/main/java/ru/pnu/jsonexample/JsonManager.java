/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.pnu.jsonexample;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Класс - утилита  для работы с форматом JSON
 *
 * @author developer
 */
public class JsonManager {

   
    /**
     * Определение основного класса для работы с технологией JSON
     */
    private static final ObjectMapper mapper = JsonMapper.builder() // создание фабрики для класса ObjectMapper
            .enable(SerializationFeature.INDENT_OUTPUT)     // Форматированный вывод JSON             
            .registerSubtypes(Student.class)                // Регистрация типа данных Student
            .registerSubtypes(Professor.class)              // Регистрация типа  данных Professor
            .build();

    
    /**
     * Преобразование экземпляра класса в формат JSON
     *
     * @param obj Объект преобразуемый в формат JSON
     * @return строка, содержащая экземпляр класса в формате JSON
     */
    public static Optional<String> toJsonFormat(Object obj) {
        Optional<String> opt = Optional.empty();
        if (obj != null) {
            try {
                String json = mapper.writeValueAsString(obj);
                opt = Optional.of(json);
            } catch (JsonProcessingException ex) {
                String error = "Ошибка преобразования в JSON формат: " + ex.toString();
                System.out.println(error);
            }
        }
        return opt;
    }

    
    
    /**
     * Метод записи экземпляра в файл формата JSON
     * 
     * @param obj
     * @param file
     * @return 
     */
    public static boolean toJsonFormat(Object obj, File file) {
        boolean result = false; // Признак неуспешного завершения операции записи в файл
        
        if (obj != null) {
            try {
                mapper.writeValue(file, obj);  
                result = true;                
            } catch (JsonProcessingException ex) {
                String error = "Ошибка преобразования в JSON формат: " + ex.toString();
                System.out.println(error);                
            } catch (IOException ex) {
                String error = "Ошибка записи экземпляра в файл ["+file.getName()+"] JSON формата: " + ex.toString();
                System.out.println(error);
            }
            
        }
        return result;
    }
    
    
    
    /**
     * Преобразование из строки JSON в экземпляр класса
     *
     * @param json
     * @return
     */
    public  static <J>  J fromJsonFormat(String json, Class<J> clazz) {
        J value = null;
        try {            
            value = mapper.readValue(json, clazz);
        } catch (JsonProcessingException ex) {
            System.out.println("Ошибка преобразования из строки JSON в экземпялр класса [" + clazz.getClass().getCanonicalName() + "]: " + ex.toString());
        }
        return value;
    }

    // TypeReference<T> valueTypeRef
    
    /**
     * Преобразование из строки JSON в экземпляр класса
     *
     * @param json
     * @return
     */
    public  static <T>  T fromJsonFormat(File file, TypeReference<T> typeRef) {
        T value = null;       
        try {            
            value = mapper.readValue(file , typeRef);
        } catch (JsonProcessingException ex) {
            System.out.println("Ошибка преобразования JSON:"+ex.toString());
        } catch (IOException ex) {
            String error = "Ошибка чтения экземпляра в файл ["+file.getName()+"] JSON формата: " + ex.toString();
            System.out.println(error);
        }
        return value;
    }
    
    

}
