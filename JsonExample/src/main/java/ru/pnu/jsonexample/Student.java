/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.pnu.jsonexample;

/**
 *
 * @author developer
 */
public class Student {

   
    
    private String firstName;       // Имя студента    
    private String sureName;        // Фамилия студента
    private String middleName;      // Отчетсво студента
    private int yearOfStudy;        // Год обучения (курс)
    private String studentGroup;    // Имя группы

   // private Professor projectManager;   // Руководитель ДП
    
    
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Экземпляр класса СТУДЕНТ: {");
        sb.append("firstName=").append(firstName);
        sb.append(", sureName=").append(sureName);
        sb.append(", middleName=").append(middleName);
        sb.append(", yearOfStudy=").append(yearOfStudy);
        sb.append(", studentGroup=").append(studentGroup);
        sb.append('}');
        return sb.toString();
    }

//    public Professor getProjectManager() {
//        return projectManager;
//    }
//
//    public void setProjectManager(Professor projectManager) {
//        this.projectManager = projectManager;
//    }
    
    
    
    

    /**
     * Get the value of sureName
     *
     * @return the value of sureName
     */
    public String getSureName() {
        return sureName;
    }

    /**
     * Set the value of sureName
     *
     * @param sureName new value of sureName
     */
    public void setSureName(String sureName) {
        this.sureName = sureName;
    }

 /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return the yearOfStudy
     */
    public int getYearOfStudy() {
        return yearOfStudy;
    }

    /**
     * @param yearOfStudy the yearOfStudy to set
     */
    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    /**
     * @return the studentGroup
     */
    public String getStudentGroup() {
        return studentGroup;
    }

    /**
     * @param studentGroup the studentGroup to set
     */
    public void setStudentGroup(String studentGroup) {
        this.studentGroup = studentGroup;
    }
    
    
    
    /**
     * Get the value of firstName
     *
     * @return the value of firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the value of firstName
     *
     * @param firstName new value of firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    
}
