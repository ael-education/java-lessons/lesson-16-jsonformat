/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package ru.pnu.jsonexample;

import com.fasterxml.jackson.core.type.TypeReference;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author developer
 */
public class JsonExample {

    public static void main(String[] args) {
        System.out.println("Изучение формата JSON");
        
        File file = new File("/home/developer/temp/professors.txt");
        
        Student st1 = new Student();
        st1.setSureName("Кузьминов");
        st1.setFirstName("Данила");
        st1.setMiddleName("Андреевич");
        st1.setYearOfStudy(2);
        st1.setStudentGroup("ПИ(б)-21");
        
        Student st2 = new Student();
        st2.setSureName("Попов");
        st2.setFirstName("Евгений");
        st2.setMiddleName("Вячеславович");
        st2.setYearOfStudy(2);
        st2.setStudentGroup("МР-232");
        
        Student st3 = new Student();
        st3.setSureName("Юшкевич");
        st3.setFirstName("Арина");
        st3.setMiddleName("Константиновна");
        st3.setYearOfStudy(2);
        st3.setStudentGroup("ПИ(б)-21");
        
        
        
        // Список преподавателей 
        List<Professor> professorList = new ArrayList<>();
        
        Professor prof1 = new Professor();
        prof1.setFirstName("Иван");
        prof1.setSureName("Петров");
        prof1.setMiddleName("Петрович");
        prof1.setDepartment("Кафедра цифрового, государственного и корпоративного управления");
        prof1.setPosition("Профессор");
        
        
        Professor prof2 = new Professor();
        prof2.setFirstName("Дмитрий");
        prof2.setSureName("Ваганов");
        prof2.setMiddleName("Валерьевич");
        prof2.setDepartment("Кафедра цифрового, государственного и корпоративного управления");
        prof2.setPosition("Старший преподаватель");
        
        // Добавление списка студентов в список дисплаников у каждого преподавателя
        //prof2.getGraduatorList().add(st1);
        prof2.getGraduatorList().add(st3);
        
        prof1.getGraduatorList().add(st2);
        prof1.getGraduatorList().add(st3);
        
        // Формирование списка преподавателей
        professorList.add(prof2);
        professorList.add(prof1);
        
        
        System.out.println("Экземпляр класса Student в формате строки:");
        System.out.println(st1);
        
        Optional<String> studentOpt = JsonManager.toJsonFormat(st1);
        Optional<String> professorOpt = JsonManager.toJsonFormat(prof1);
        
        if (studentOpt.isPresent()) {
            System.out.println("Экземпляр класса Student в формате JSON:");
            System.out.println(studentOpt.get());
        }
        
        
        if (professorOpt.isPresent())
        {    
         System.out.println("Экземпляр класса Professor в формате JSON:");
         System.out.println(professorOpt.get());
        }       
        
        System.out.println("Обратное преобразование из формата JSON ...");        
        System.out.println("Обратное преобразование из формата JSON для экземпляра Student ...");        
        Student s1 =  JsonManager.fromJsonFormat(studentOpt.get(), Student.class);
        System.out.println("Экземпляр класса Student в формате строки:");
        System.out.println(s1);
        
        System.out.println("Обратное преобразование из формата JSON ...");        
        System.out.println("Обратное преобразование из формата JSON для экземпляра Student ...");        
        Professor p1 =  JsonManager.fromJsonFormat(professorOpt.get(), Professor.class);
        System.out.println("Экземпляр класса Student в формате строки:");
        System.out.println(p1.toString());
        
        System.out.println("Запись списка экзмепляров класса Professor в файл: "+file.getAbsolutePath());
        boolean result = JsonManager.toJsonFormat(professorList, file);
        
        if (result)
        {
            System.out.println("Список экзмепляров класса Professor успешно записан в файл: "+file.getAbsolutePath());
        }
        
        System.out.println("Чтение  списка экзмепляров класса Professor из файла: "+file.getAbsolutePath());
        TypeReference<List<Professor>> typeRef = new TypeReference<List<Professor>>() {};
        
        List<Professor> profList =  (List<Professor>) JsonManager.fromJsonFormat(file, typeRef);
        System.out.println("Прочитано из файла ["+profList.size()+"] экземпляров класса Professor");
        
        
        Professor foundProfessor = null;
        
        String firstName = "Дмитрий";
        String sureName = "Иванов";
        
        
        
        
        // Цикл просмотра списка преподвателей
        /*
        for (Professor professor : profList) {
            
            if (professor.getSureName().equalsIgnoreCase(sureName))
            {
                foundProfessor = professor; // Найден профессор Петров
            }   
            
        }
        
        if (foundProfessor != null) {
            System.out.println("Надена запись: " + foundProfessor);
        } else {
            System.out.println("Запись по фамилии: " + sureName + " не найдена");
        }
        */
        
        
        
       
          // Поиск в загруженном массиве 
        List<Professor> foundList = profList
                .parallelStream()   // Преобрзование в поток
                //filter(professor -> professor.getSureName().equalsIgnoreCase(sureName))
                //.filter(professor -> professor.getSureName().equalsIgnoreCase(sureName) && professor.getFirstName().equalsIgnoreCase(firstName))
                .filter(professor -> professor.getGraduatorList().size() == 2)
                .collect(Collectors.toList());

        System.out.println("Найдено [" + foundList.size() + "] экземпляров класса Professor");
        if (!foundList.isEmpty()) {

            System.out.println(foundList.get(0));
        }
       
        
        
      
        
        
        
    }
}
